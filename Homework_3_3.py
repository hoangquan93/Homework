import numpy as np
n = input("Input 'M,N,K' of 3D array ")
#make a list of input
a = list(map(int,n.split(",")))
m = int(a[0])
n = int(a[1])
k = int(a[2])

# Generate M*N*K values on interval [-100,100]
arr = np.random.uniform(-100,100,[m*n*k])

print(arr)
print("Min of deep of array: \n", arr.min(axis=0))
print("Max of deep of array: \n",arr.max(axis=0))
print("Sum of deep of array: \n", arr.sum(axis=0))

print("Min of height of array: \n", arr.min(axis=1))
print("Max of height of array: \n",arr.max(axis=1))
print("Sum of height of array: \n", arr.sum(axis=1))

print("Min of width of array: \n", arr.min(axis=2))
print("Max of width of array: \n",arr.max(axis=2))
print("Sum of width of array: \n", arr.sum(axis=2))