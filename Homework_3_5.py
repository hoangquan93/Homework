import numpy as np
# Financial functions
"""rate = 0.06 # annually
nper = 12 # months
per = 8
pmt = 100000 or -1000000
pv = 20000000 or -20000000
when = 'end' """
# Futere value after 12 months
fv = np.fv(0.06/12,12,-1000000,-20000000)
print(fv)

#  NPV (Net Present Value), assume value = 23,000,000, 1 year
npv = np.npv(0.06,[-21000000,23000000])
print("npv: ", npv)

""" the monthly payment needed to pay off a 20,000,000 loan
 in 1 years at an annual interest rate of 6.0 % annually"""
pmt = np.pmt(0.06/12,12,20000000)
print("pmt:  ",pmt)

""" the 8th month payment(not inclue interest)  needed to pay off a 
20,000,000 loan in 1 years at an annual interest rate of 6.0 %"""
ppmt = np.ppmt(0.06/12,8,12,20000000)
print("ppmt:  ",ppmt)

""" the 8th month of the interest portion of a payment needed to pay off
a 20,000,000 loan in 1 years at an annual interest rate of 6.0 % """
ipmt = np.ipmt(0.06/12,8,12,20000000)
print("ipmt:  ",ipmt)

irr = np.irr([-20000000,22000000])
print("irr:  ",irr)
