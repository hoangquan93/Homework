import pandas as pd
import random

x = pd.date_range(start='1/1/2018', periods=50, freq='M')
a = [random.choice(range(10,30)) for i in range(0,50)]
b = pd.DataFrame({'month':x,'sale':a})
b['year'] = x.year
c = b.groupby('year').sum()
print("Dataframe: \n", b)
print("Sum of sale by year: \n", c["sale"])



