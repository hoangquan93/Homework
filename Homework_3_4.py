import numpy as np
# get a float number from user and find its closest values in the array
a = float(input("Input your number: "))
# generate a 2D array 10 x 8
x = np.random.uniform(-10,10,[80])
arr = x.reshape(10,8)
print("Randomly 2D array 10 x 8 of float values on interval [-10, 10]: \n",arr)

# find its closest values in the array
index = (np.abs(a-x)).argmin()
print("Closest values to",a,":  ",x[index])

# find 3 closest values in the array
x1 = x[x != x[index]] # remove x[index] from x
index1 = (np.abs(a-x1)).argmin()
x2 = x1[x1 != x1[index1]] # remove x1[index1] from x1
index2 = (np.abs(a-x2)).argmin()
print("3 closest values to",a,"in the array: ", x[index],  x1[index1],  x2[index2])