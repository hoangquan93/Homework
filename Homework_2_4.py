n = int(input("Enter an integer in range(0,10):  "))
# randomly generate a list of N integers
import random
x = [random.randint(0,10) for x in range(n)]

def histogram(lst):
    for n in lst :
        output = ""
        times = n
        while( times > 0 ):
            output += "*"
            times = times - 1
        print(output)
# Create and print a histogram of the list
print("Random list:  ", x)
print("Histogram of the list:")
histogram(x)

# Manually calculate and print the statistics description of the list
print("Max of list: ", max(x))
print("Min of list: ", min(x))
print("Mean of list: ", sum(x)/n)

def var_method(lst):
    n = len(lst)
    mean = sum(lst) / float(n)
    return sum((mean - x) ** 2 for x in lst) / float(n - 1)
print("Variance of list:  ", var_method(x))
import math
print("Standard deviation:  ", math.sqrt(var_method(x)))

# Remove duplicates from the list
print("Remove duplicates from the list", list(set(x)))