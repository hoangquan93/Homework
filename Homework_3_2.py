import numpy as np
n = int(input("Input N: "))
arr = np.random.uniform(0,10,[n*n*n])

print(arr)
print("Min of deep of array: \n", arr.min(axis=0))
print("Max of deep of array: \n",arr.max(axis=0))
print("Sum of deep of array: \n", arr.sum(axis=0))

print("Min of height of array: \n", arr.min(axis=1))
print("Max of height of array: \n",arr.max(axis=1))
print("Sum of height of array: \n", arr.sum(axis=1))

print("Min of width of array: \n", arr.min(axis=2))
print("Max of width of array: \n",arr.max(axis=2))
print("Sum of width of array: \n", arr.sum(axis=2))