import pandas as pd
import random
ids = ["id1","id2","id3","id4","id5","id6","id7","id8","id9","id10","id11","id12","id13","id14","id15","id16","id17","id18","id19","id20"]
s1 = random.sample(ids, 10)
s2 = random.sample(ids, 10)
# make S1,S2,S3,S4
S1 = pd.Series(s1)
S2 = pd.Series(s2)
S3 = random.sample(range(-100,100), 10)
S4 = random.sample(range(-100,100), 10)

# Build 2 DataFrame: DF1,DF2
DF1 = pd.DataFrame({'key': S1, 'data1':S3})
DF2 = pd.DataFrame({'key': S2, 'data2':S4})

# Make DF
DF = pd.merge(DF1, DF2, on = 'key')



