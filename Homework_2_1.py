value = input('Input some comma seprated integer numbers:  ')
# make list from input
mylist = list(map(int,value.split(",")))
even_numbers = [x for x in mylist if x % 2 == 0]
odd_numbers = [x for x in mylist if x % 2 != 0]
print('List of even numbers:  ', even_numbers)
print('List of odd numbers:  ', odd_numbers)
