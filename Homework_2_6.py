M = [[1,2,3,4,5],
        [3,4,2,5,6],
        [1,6,3,2,5]]

def sum_Column(m):
    return [sum(col) for col in zip(*m)]
print("Sum of column of matrix: ", sum_Column(M))